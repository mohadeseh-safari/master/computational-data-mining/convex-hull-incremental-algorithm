# Point model
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


# Quick sort algorithm in ascending order with O(nlog(n)) time complexity
def quicksort(arr, attr):
    if len(arr) <= 1:
        return arr
    else:
        pivot = getattr(arr[0], attr)
        lesser = [x for x in arr[1:] if getattr(x, attr) <= pivot]
        greater = [x for x in arr[1:] if getattr(x, attr) > pivot]
        return quicksort(lesser, attr) + [arr[0]] + quicksort(greater, attr)


def right_turn(p1, p2, p3):
    # Calculate the cross product
    cross_product = (p2.x - p1.x) * (p3.y - p2.y) - (p2.y - p1.y) * (p3.x - p2.x)
    return cross_product < 0  # Right turn if cross product is negative


def incremental_convex_hull(points):
    # Sorting the points from left to right
    p = quicksort(points, 'x')
    n = len(p)
    l_upper = [p[0], p[1]]
    l_lower = [p[n - 1], p[n - 2]]

    # Building upper convex
    for j in range(2, n):
        l_upper.append(p[j])
        size = len(l_upper)
        while size > 2 and not right_turn(l_upper[size - 3], l_upper[size - 2], l_upper[size - 1]):
            l_upper.pop(size - 2)
            size = len(l_upper)

    # Building lower convex
    for j in reversed(range(0, n - 2)):
        l_lower.append(p[j])
        size = len(l_lower)
        while size > 2 and not right_turn(l_lower[size - 3], l_lower[size - 2], l_lower[size - 1]):
            l_lower.pop(size - 2)
            size = len(l_lower)

    l_lower.pop(0)
    l_lower.pop(len(l_lower) - 1)
    return l_lower + l_upper


# Example
# Creating a list of pints
points = [
    Point(2, 3),
    Point(1, 2),
    Point(2, 1),
    Point(4, 0),
    Point(4, -1),
    Point(2, -1),
    Point(1, -2),
    Point(0, -3),
    Point(-1, -1),
    Point(-3, -1),
    Point(-2, 1),
    Point(-1, 2),
]

convex_hull = incremental_convex_hull(points)
print("Convex hull:")
for p in convex_hull:
    print(f'({p.x},{p.y})')
