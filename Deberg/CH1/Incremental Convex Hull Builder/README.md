
# Convex hull using incremental algorithm

The implementation of an incremental algorithm to compute convex hull of a set of points.

## Psudo code
**Algorithm** ConvexHull(P)   
**Input:** A set `P` of points in the plane.   
**Output:** A list containing the vertices of `CH(P)` in clockwise order.   
* Sort the points by x-coordinate, resulting in a sequence `p₁, ..., pₙ`.  
* Put the points `p₁` and `p₂` in a list `Lᵤₚₚₑᵣ`, with `p₁` as the first point.
* For `i` from 3 to `n`:
   - Append `pᵢ` to `Lᵤₚₚₑᵣ`.
   - While `Lᵤₚₚₑᵣ` contains more than two points and the last three points in `Lᵤₚₚₑᵣ` do not make a right turn:
     - Delete the middle of the last three points from `Lᵤₚₚₑᵣ`.
* Put the points `pₙ` and `pₙ₋₁` in a list `Lₗₒwₑᵣ `, with `pₙ` as the first point.
* For `i` ← `n - 2` downto 1:
   - Append `pᵢ` to `Lₗₒwₑᵣ `.
   - While `Lₗₒwₑᵣ ` contains more than 2 points and the last three points in `Lₗₒwₑᵣ ` do not make a right turn:
     - Delete the middle of the last three points from `Lₗₒwₑᵣ `.
* Remove the first and the last point from `Lₗₒwₑᵣ ` to avoid duplication of the points where the upper and lower hull meet.
* Append `Lₗₒwₑᵣ ` to `Lᵤₚₚₑᵣ`, and call the resulting list `L`.
* Return `L`.

## Example
The set of points shown in the following image are used as an input example in code.

![Alt text](example.png)
